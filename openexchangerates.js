/** 
 * @file openexchangerates
 * @version 0.1.0
 * @author Tony Aguilar
 * @copyright 2015
*/

 /** openexchangerates manager
 * @module openexchangerates
 */
var fs = require("fs");
var superagent = require('superagent');
var _ = require('underscore');
var jstrace = require('jstrace');

var EventEmitter = require('events').EventEmitter;

var exportedObject;

const UPDATE_FREQUENCY = (60 * 60 * 1000);
const FAST_UPDATE_FREQUENCY = (60 * 1000);

/** Creates a new OpenExchangeRatesConfig object
 * @constructor
 * @property {string} appId APP ID @ openexchangerates.org<br>
 * @property {boolean} autoUpdate Auto-update rates?
 * @property {boolean} debug Show debug messages?
 */
OpenExchangeRatesConfig = function() {
	this.appId = 'NOT_SET';
	this.autoUpdate = true;
	this.debug = false;
};

/** Creates a new OpenExchangeRates manager
 * @constructor
 * @param {OpenExchangeRatesConfig} config Configuration options
 */
var OpenExchangeRates = function(config) {
	this.config = config;
	this.url = 'http://openexchangerates.org/api/latest.json?app_id=' + this.config.appId;
	this.currencies = require("./currencies.json");
	this.emitter = new EventEmitter();

	try {
		this.rateData = require("./latest.json");
	}
	catch (err) {
		this.rateData = {_timestamp:0,timestamp: "",base:"",rates:{}};
	}

	this.rateData.timestamp = new Date(this.rateData._timestamp * 1000);

	this.checkUpdates();
};

/** Debug message handler.<br>
 * Shows the debug message ONLY if debug is enabled in configuration.<br>
 * @param {string} method jstrace method to use
 * @param ... Same parameters as for console.log()
 */
OpenExchangeRates.prototype.dbg = function() {
	if( this.config.debug != true ) {
		return;
	}

	var args = Array.prototype.slice.apply(arguments);
	var method = args.shift();

	jstrace[method].apply(null,args);
}

/** Checks if an update is necessary.<br>
 * If it is, it does the request. The request will emit an event when finished.<br>
 * If saved rates are good enough, it will skip the request.
 */
OpenExchangeRates.prototype.checkUpdates = function() {
	if( this.config.autoUpdate === false ) {
		this.dbg('i', '[oer] checkUpdates() - autoUpdate is disabled, skipping update request.');
		return;
	}

	var now = new Date();
	var elapsed = now - this.rateData.timestamp;

	if( elapsed > UPDATE_FREQUENCY ) {
		this.dbg('i', '[oer] checkUpdates() - Requesting new info...');
		this.request();
	}
	else {
		this.dbg('i', '[oer] checkUpdates() - Info is new enough, skipping update request.');
		var nextRun = (UPDATE_FREQUENCY - elapsed);
		var nextRunDate = new Date(now.valueOf()+nextRun);
		this.dbg('i', '[oer] checkUpdates() - Next auto-update @ %s', nextRunDate);
		setTimeout( this.checkUpdates.bind(this), nextRun );
	}
	
};

/** Sends a request to the service, parses the result
 * and saves the "cache" file.
 * @emits update
 * @emits error
*/
OpenExchangeRates.prototype.request = function() {
	this.dbg('i', '[oer] request() - Starting request...');

	superagent.get(this.url, (error, res) => {
		// If error happens, ignore it because we'll try again in an hour
		if (error) {
			this.emitter.emit('error', error);
			return;
		}

		var results;
		try {
			this.dbg('i', '[oer] request() - Response received, parsing response...');
			var results = JSON.parse(res.text);
			var nextRunDate;
			var nextRun;

			// Cancel operation if received timestamp is same as ours
			if( this.rateData._timestamp == results.timestamp )
			{
				this.dbg('w', '[oer] request() - CANCELLING - timestamp match!');
				nextRunDate = new Date(Date.now().valueOf()+FAST_UPDATE_FREQUENCY);
				this.dbg('w', '[oer] request() - Next auto-update (FAST) @ %s', nextRunDate);
				setTimeout( this.checkUpdates.bind(this), FAST_UPDATE_FREQUENCY );
				return;
			}

			// Copy received data into rateData
			this.rateData._timestamp = results.timestamp;
			this.rateData.base = results.base;

			// Copy all rates
			_.each(results.rates || {}, (value, key) => { this.rateData.rates[key] = value; });

			// Regenerate date object
			this.rateData.timestamp = new Date(this.rateData._timestamp * 1000);
			this.dbg('d', '[oer] request() - New timestamp=%s',this.rateData.timestamp);

			// Write received exchange data
			this.dbg('i', '[oer] request() - Writing latest.json file...');
			fs.writeFile( __dirname + "/latest.json", JSON.stringify( this.rateData ), "utf8", (error) => {
				if(error)
					this.emitter.emit('error', error);
				else
					this.emitter.emit('update');
			});

			// Program next auto-update
			nextRunDate = new Date(this.rateData.timestamp.valueOf() + UPDATE_FREQUENCY);
			this.dbg('i', '[oer] request() - Next auto-update @ %s', nextRunDate);
			nextRun = (nextRunDate.valueOf() - Date.now().valueOf());
			setTimeout( this.checkUpdates.bind(this), nextRun );

		} catch (e) {
			this.emitter.emit('error', e);
		}
	});
};

/** Returns the last update timestamp
 * @returns {Date} Timestamp
 */
var getTimestamp = function() { return exportedObject.rateData.timestamp; };

/** Returns the exchange base currency
 * @returns {string} International-standard 3-letter currency code
 */
var getBase = function() { return exportedObject.rateData.base; };

/** Returns the exchange rate of a given currency
 * @param {string} currency International-standard 3-letter currency code
 * @returns {Number} Exchange rate
 */
var getRate = function(currency) { return exportedObject.rateData.rates[currency]; };

/** Returns the exchange rates
 * @returns {Object} Rates object (see "rates" [here]{@link https://openexchangerates.org/documentation})
 */
var getRates = function() { return exportedObject.rateData.rates; };

/** Returns the long name of a given currency
 * @param {string} currency International-standard 3-letter currency code
 * @returns {String} Currency long name
 */
var getName = function(currency) { return exportedObject.currencies[currency]; };

/** Returns the currency long names
 * @returns {Object} Long names in an object (see [this]{@link https://openexchangerates.org/api/currencies.json})
 */
var getNames = function() { return exportedObject.currencies; };

/** Sets <b>currency_exchange</b> configuration and starts the auto updates (if configured).<br>
 * The return value is an object containing an event emitter and some callable methods.
 * @param {OpenExchangeRatesConfig} config Configuration options
 * @returns {Object} Object containing:<br>
 * <ul>
 * 	<li>Event emitter:</li>
 * 	<ul>
 * 		<li>events: emits 'error' and 'update' events</li>
 * 	</ul>
 * 	<li>Callable methods:</li>
 * 	<ul>
 * 		<li>getRate()</li>
 * 		<li>getRates()</li>
 * 		<li>getName()</li>
 * 		<li>getNames()</li>
 * 		<li>getTimestamp()</li>
 * 		<li>getBase()</li>
 * 	</ul>
 * </ul>
 * <br>
 * For more information about this data, please see [this]{@link https://openexchangerates.org/documentation}
 */
module.exports = function(config) {
	exportedObject = new OpenExchangeRates(config);

	return {
		/** Use this to subscribe to events such as 'update' or 'error' 
		 * @emits update
		 * @emits error
		 */
		events: exportedObject.emitter,
		getRate: getRate,
		getRates: getRates,
		getName: getName,
		getNames: getNames,
		getTimestamp: getTimestamp,
		getBase: getBase,
	};
};
