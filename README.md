# openexchangerates #

Node.js currency exchange manager for [openexchangerates.org](http://openexchangerates.org)

* Automatically updates every hour (configurable).
* Keeps a "cache" file to avoid unnecessary HTTP requests.
* You'll need an APP ID from openexchangerates, get it [here](https://openexchangerates.org/signup)

### Files ###

* **openexchangerates.js** : Main js file
* **currencies.json** : Currency long names (from openexchangerates.org)
* **latest.json** : Currency exchange "cache" (generated on first run)

### How to install it ###

```
npm install --save git+http://bitbucket.org/clvLabs/node_openexchangerates.git
```

### How to use it ###

Please note that as latest.json is generated on the first run, you'll see a NaN as any rate you ask on this first run before the first update is received (luckily, that's fast enough...).

Simple version
```
var oer = require('openexchangerates')({appId: 'your_app_id'});

console.log('Rates last udpated @ %s', oer.getTimestamp());
console.log('1 EUR (%s) = %d USD (%s)', oer.getName('EUR'), 1/oer.getRate('EUR'), oer.getName('USD'));
```

Adding update and error notifications
```
var oer = require('openexchangerates')({appId: 'your_app_id'});

oer.events.on('update', function() {
	console.log('-------------------');
	console.log('NEW RATES udpated @ %s', oer.getTimestamp());
	console.log('1 EUR (%s) = %d USD (%s)', oer.getName('EUR'), 1/oer.getRate('EUR'), oer.getName('USD'));
});

oer.events.on('error', function(error) {
	console.log(error);
});

console.log('Rates last udpated @ %s', oer.getTimestamp());
console.log('1 EUR (%s) = %d USD (%s)', oer.getName('EUR'), 1/oer.getRate('EUR'), oer.getName('USD'));
```

### How to generate docs ###

Go to (your_project_folder)/node_modules/openexchangerates/ and execute
```
npm install jsdoc
./node_modules/bin/jsdoc openexchangerates.js readme.md -d docs --verbose
```
This will create a **docs** folder containing the documentation.
